﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using visabymastersAPI.Models;

namespace visabymastersAPI.Service.Interfaces
{
    public interface IVisaDetailsService
    {
        Task<dynamic> GetGeneralInfo (int visaRequirmentId);

        Task InsertVisaRequirementGeneralInfo ( EmbassyGeneralInfo embassyGeneralInfo );
    }
}
