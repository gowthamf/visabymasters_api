﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using visabymastersAPI.Models;

namespace visabymastersAPI.Controllers
{
    //[Authorize]
    [Route ("api/[controller]")]
    [ApiController]
    [EnableCors ("AllowOrigins")]
    public class CountryController :ControllerBase
    {
        VisaByMastersDbContext _context;
        public CountryController ( VisaByMastersDbContext context )
        {
            _context = context;


        }


        [HttpGet ("GetCountires")]
        public async Task<IEnumerable<Country>> GetCountriesAsync ( )
        {

            return await _context.Countries.ToListAsync ();
        }

        [HttpPost ("InsertCountry")]

        public async Task<IActionResult> InsertCountryAsync ( Country country )
        {
            await _context.Countries.AddAsync (country);
            await _context.SaveChangesAsync ();

            return Created (Request.Path + $"/{country.Id}", country);
        }

        [HttpPut ("UpdateCountry/{id}")]
        public async Task<IActionResult> UpdateCountryAsync ( Country country, int id )
        {
            var _country = await _context.Countries.Where (c => c.Id == id).FirstOrDefaultAsync ();

            if ( _country != null )
            {
                _country.CountryName = country.CountryName;
                _country.CountryShortName = country.CountryShortName;
                _country.CountryFlagPath = country.CountryFlagPath;

                await _context.SaveChangesAsync ();

                return Ok (_country);
            }

            return NotFound ($"Country not found");

        }

        [HttpDelete ("DeleteCountry/{id}")]
        public async Task<IActionResult> DeleteCountryAsync ( int id )
        {
            var _country = await _context.Countries.Where (c => c.Id == id).FirstOrDefaultAsync ();

            if ( _country != null )
            {
                _context.Countries.Remove (_country);
                await _context.SaveChangesAsync ();
                return Ok (_country);
            }

            return NotFound ($"Country not found");
        }





    }
}