﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using visabymastersAPI.Models;
using Microsoft.EntityFrameworkCore;

namespace visabymastersAPI.Controllers
{
    [Route ("api/[controller]")]
    [ApiController]
    public class VisaRequirementController :ControllerBase
    {
        VisaByMastersDbContext _context;
        public VisaRequirementController ( VisaByMastersDbContext context)
        {
            _context = context;
        }

        [HttpGet ("GetCountryVisaRequirements/{toCountryId}/{fromCountryId}")]
        public async Task<VisaRequirement> GetVisaRequirementAsync ( int toCountryId, int fromCountryId )
        {
            var visaInfo = await _context.VisaRequirements.Include (v => v.FromCountry).Include (v => v.ToCountry).Where (v => v.ToCountryId == toCountryId && v.FromCountryId == fromCountryId).FirstOrDefaultAsync ();

            return visaInfo;
        }

        [HttpPost ("InsertCountryVisaRequirements")]
        public async Task<IActionResult> InsertVisaRequirementsAsync ( VisaRequirement visaRequirement)
        {
            await _context.VisaRequirements.AddAsync (visaRequirement);

            await _context.SaveChangesAsync ();

            return Created (Request.Path + $"/{visaRequirement.FromCountryId}", visaRequirement.Id);
        }

        [HttpPut ("UpdateCountryVisaRequirements")]
        public async Task<IActionResult> UpdateCountry ( VisaRequirement updateVisaRequirement )
        {
            var dbVisaCountry = await _context.VisaRequirements.Include (v => v.FromCountry).Include (v => v.ToCountry).Where (v => v.ToCountryId == updateVisaRequirement.ToCountryId && v.FromCountryId == updateVisaRequirement.FromCountryId).FirstOrDefaultAsync ();

            if ( dbVisaCountry != null )
            {
                dbVisaCountry.IsVisaFree = updateVisaRequirement.IsVisaFree;
                dbVisaCountry.IsVisaOnArrival = updateVisaRequirement.IsVisaOnArrival;
                dbVisaCountry.IsVisaRequired = updateVisaRequirement.IsVisaRequired;

                await _context.SaveChangesAsync ();

                return Ok ("Updated");
            }

            return NotFound ();

        }

        [HttpDelete ("DeleteCountryVisaRequirements/{toCountryId}/{fromCountryId}")]
        public async Task<IActionResult> DeleteCountry ( int toCountryId, int fromCountryId )
        {
            var visaInfo = await _context.VisaRequirements.Include (v => v.FromCountry).Include (v => v.ToCountry).Where (v => v.ToCountryId == toCountryId && v.FromCountryId == fromCountryId).FirstOrDefaultAsync ();

            if ( visaInfo != null )
            {
                _context.VisaRequirements.Remove (visaInfo);
                await _context.SaveChangesAsync ();

                return Ok ("Deleted");
            }

            return NotFound ();
        }

    }
}