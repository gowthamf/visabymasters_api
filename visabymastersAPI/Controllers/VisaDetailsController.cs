﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using visabymastersAPI.Models;
using visabymastersAPI.Service.Interfaces;

namespace visabymastersAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VisaDetailsController : ControllerBase
    {
        IVisaDetailsService _visaDetailsService;
        public VisaDetailsController (IVisaDetailsService visaDetailsService,IHostingEnvironment hostingEnvironment )
        {
            _visaDetailsService = visaDetailsService;
        }

        [HttpGet]
        [Route("GetGeneralInfo/{visaReqId}")]
        public async Task<dynamic> GetGeneralInfo ( int visaReqId)
        {
            return await _visaDetailsService.GetGeneralInfo (visaReqId);
        }

        [HttpPost]
        [Route ("InsertGeneralInfo")]
        public async Task InsertGeneralInfo ( EmbassyGeneralInfo embassyGeneralInfo )
        {
            await _visaDetailsService.InsertVisaRequirementGeneralInfo (embassyGeneralInfo);
        }
    }
}