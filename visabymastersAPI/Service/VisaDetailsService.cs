﻿using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using visabymastersAPI.Models;

namespace visabymastersAPI.Service.Interfaces
{
    public class VisaDetailsService :IVisaDetailsService
    {
        private readonly IHostingEnvironment _hostingEnvironment;

        public VisaDetailsService (IHostingEnvironment hostingEnvironment )
        {
            _hostingEnvironment = hostingEnvironment;
        }

        public async Task InsertVisaRequirementGeneralInfo ( EmbassyGeneralInfo embassyGeneralInfo )
        {
            await InsertGeneralInfo (embassyGeneralInfo);
        }

        async Task<dynamic> IVisaDetailsService.GetGeneralInfo ( int visaReqId)
        {
            return await GetGeneralInformation (visaReqId);
        }

		async Task<dynamic> GetGeneralInformation ( int visaRequirementId)
        {
            string rootPath = _hostingEnvironment.ContentRootPath;
            var json = await System.IO.File.ReadAllTextAsync ($"{rootPath}/visa-information.json");
            var obj = JsonConvert.DeserializeObject<List<EmbassyGeneralInfo>> (json).Where(v=>v.VisaRequirementId==visaRequirementId);
            return obj;
        }

		async Task InsertGeneralInfo (EmbassyGeneralInfo embassyGeneralInfo)
        {
            string rootPath = _hostingEnvironment.ContentRootPath;
            var json = await System.IO.File.ReadAllTextAsync ($"{rootPath}/visa-information.json");
            var obj = JsonConvert.DeserializeObject<List<EmbassyGeneralInfo>> (json);
            obj.Add (embassyGeneralInfo);
            var jsonString = JsonConvert.SerializeObject (obj);
            await System.IO.File.WriteAllTextAsync ($"{rootPath}/visa-information.json", jsonString);
        }
    }
}
