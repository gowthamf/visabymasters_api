﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace visabymastersAPI.Models
{
    public class Users
    {
        public int Id { get; set; }
        public string UserName { get; set; }

        public string Email { get; set; }

        public byte [] PasswordHash { get; set; }

        public byte [] PasswordSalt { get; set; }

        public DateTime DateOfBirth { get; set; }

        public string Gender { get; set; }

        public string City { get; set; }

        public string Country { get; set; }

    }
}
