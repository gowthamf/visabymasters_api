﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace visabymastersAPI.Models
{
    public class VisaByMastersDbContext:DbContext
    {
        public VisaByMastersDbContext (DbContextOptions<VisaByMastersDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<VisaRequirement>()
                 .Property(r => r.IsVisaFree)
                 .HasConversion(new BoolToZeroOneConverter<Int16>());
            builder.Entity<VisaRequirement>()
                 .Property(r => r.IsVisaOnArrival)
                 .HasConversion(new BoolToZeroOneConverter<Int16>());
            builder.Entity<VisaRequirement>()
                 .Property(r => r.IsVisaRequired)
                 .HasConversion(new BoolToZeroOneConverter<Int16>());

            builder.Entity<Country>()
                .Property(r => r.IsPopularVisa)
                .HasConversion(new BoolToZeroOneConverter<Int16>());
        }
        public DbSet<Users> Users { get; set; }

        public DbSet<Country> Countries { get; set; }

        public DbSet<VisaRequirement> VisaRequirements { get; set; }

        public DbSet<EmbassyGeneralInfo> EmbassyGeneralInfos { get; set; }

    }
}