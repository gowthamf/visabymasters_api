﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace visabymastersAPI.Models
{
    public class VisaRequirement
    {
        public int Id { get; set; }

        public Country FromCountry { get; set; }

        public Country ToCountry { get; set; }

        public int FromCountryId { get; set; }

        public int ToCountryId { get; set; }

        public bool IsVisaRequired { get; set; }

        public bool IsVisaOnArrival { get; set; }

        public bool IsVisaFree { get; set; }
    }
}
