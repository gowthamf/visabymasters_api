﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace visabymastersAPI.Models
{
    public class OptionalServices
    {
        public int OptionalServiceId { get; set; }

        public string OptionalServiceName { get; set; }
    }
}
