﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace visabymastersAPI.Models
{
    public class ApplicationForm
    {
        public Country VisitingCountry { get; set; }

        public VisaType SelectedVisaType { get; set; }

        public OptionalServices SelectedOptionalService { get; set; }

        public ApplicantGeneralInformation ApplicantGeneralInformation { get; set; }

        public List<Applicant> ApplicantDetails { get; set; }
    }
}
