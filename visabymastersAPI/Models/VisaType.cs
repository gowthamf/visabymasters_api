﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace visabymastersAPI.Models
{
    public class VisaType
    {
        public int VisaTypeId { get; set; }

        public string VisaTypeName { get; set; }
    }
}
