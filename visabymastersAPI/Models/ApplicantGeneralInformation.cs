﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace visabymastersAPI.Models
{
    public class ApplicantGeneralInformation
    {
        public string EmailAddress { get; set; }

        public string ContactNumber { get; set; }

        public string HomeAddress { get; set; }

        public DateTime DepatureDate { get; set; }

        public DateTime ArrivalDate { get; set; }
    }
}
