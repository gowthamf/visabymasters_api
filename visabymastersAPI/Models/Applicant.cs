﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace visabymastersAPI.Models
{
    public class Applicant
    {
        public int ApplicantId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string CountryOfBirth { get; set; }

        public DateTime DateOfBirth { get; set; }

        public string Nationality { get; set; }

        public string PassportNumber { get; set; }

        public DateTime PassportExpiryDate { get; set; }

        public DateTime PassportIssuedDate { get; set; }
    }
}
