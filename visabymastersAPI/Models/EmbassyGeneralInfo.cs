﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace visabymastersAPI.Models
{
    public class EmbassyGeneralInfo
    {
        public int Id { get; set; }

        public int VisaRequirementId { get; set; }

        [NotMapped]
        public VisaInfo VisaInfo { get; set; }

        [NotMapped]
        public ContactDetails ContactDetails { get; set; }
    }
    
    public class VisaInfo
    {
        public List<string> GeneralInformation { get; set; }
        public TouristInfo TouristInfo { get; set; }
        public BusinessInfo BusinessInfo { get; set; }
    }

    public class TouristInfo
    {
        public List<string> MainDocuments { get; set; }
        public List<string> SponsorsDocuments { get; set; }
    }

    public class BusinessInfo
    {
        public List<string> MainDocuments { get; set; }
        public List<string> SponsorsDocuments { get; set; }
    }

    public class ContactDetails
    {
        public string EmbassyName { get; set; }

        public List<string> EmbassyContactDetails { get; set; }

        public string VisaCenterName { get; set; }

        public List<string> VisaCenterContactDetails { get; set; }
    }
}
