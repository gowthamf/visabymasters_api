﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace visabymastersAPI.Models
{
    public class Country
    {
        public int Id { get; set; }
        public string CountryName { get; set; }

        public string CountryShortName { get; set; }

        public string CountryFlagPath { get; set; }

        public bool IsPopularVisa { get; set; }
    }
}
